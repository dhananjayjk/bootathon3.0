function areaC() {
    let t11 = document.getElementById("t11");
    let t12 = document.getElementById("t12");
    let t21 = document.getElementById("t21");
    let t22 = document.getElementById("t22");
    let t31 = document.getElementById("t31");
    let t32 = document.getElementById("t32");
    let ans = document.getElementById("ans");
    var x1 = parseFloat(t11.value), y1 = parseFloat(t12.value), x2 = parseFloat(t21.value), y2 = parseFloat(t22.value), x3 = parseFloat(t31.value), y3 = parseFloat(t32.value);
    if (isNaN(x1) || isNaN(x2) || isNaN(x3) || isNaN(y1) || isNaN(y2) || isNaN(y3)) {
        ans.innerHTML = "Don't Enter any String";
        ans.style.color = "red";
    }
    else {
        var x = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)), y = Math.sqrt(Math.pow(x2 - x3, 2) + Math.pow(y2 - y3, 2)), z = Math.sqrt(Math.pow(x3 - x1, 2) + Math.pow(y3 - y1, 2));
        var s = (x + y + z) / 2;
        var areaa = Math.sqrt(s * (s - x) * (s - y) * (s - z));
        console.log(x);
        console.log(y);
        console.log(z);
        console.log(s);
        console.log(areaa.toFixed(2));
        ans.innerHTML = "The Area is :  " + (areaa.toFixed(2)).toString();
        ans.style.color = "black";
    }
}
//# sourceMappingURL=math3.js.map