var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var er = document.getElementById("error");
function add() {
    var x = parseFloat(t1.value);
    if (isNaN(x))
        er.innerHTML = "Don't enter string values";
    else {
        var c = x + Math.cos(x * Math.PI / 180);
        t2.innerHTML = "Answer is : " + c.toString();
        er.innerHTML = "";
    }
}
//# sourceMappingURL=app.js.map