var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
var er = document.getElementById("error");
if (typeof t1 === 'string' || typeof t2 === 'string') {
    er.innerHTML = "Don't enter string values";
    er.style.color = "red";
}
else {
    er.innerHTML = "";
    er.style.color = "black";
}
function add() {
    var c = parseFloat(t1.value) + parseFloat(t2.value);
    if (isNaN(c))
        er.innerHTML = "Don't enter string values";
    t3.value = c.toString();
}
function mul() {
    var c = parseFloat(t1.value) * parseFloat(t2.value);
    if (isNaN(c))
        er.innerHTML = "Don't enter string values";
    t3.value = c.toString();
}
function div() {
    var c = parseFloat(t1.value) / parseFloat(t2.value);
    if (isNaN(c))
        er.innerHTML = "Don't enter string values";
    t3.value = c.toString();
}
function sub() {
    var c = parseFloat(t1.value) - parseFloat(t2.value);
    if (isNaN(c))
        er.innerHTML = "Don't enter string values";
    t3.value = c.toString();
}
function operate() {
    var ele = document.getElementById('func');
    var i = ele.selectedIndex;
    er.innerHTML = '';
    if (i == 0) {
        add();
    }
    else if (i == 1) {
        sub();
    }
    else if (i == 2) {
        mul();
    }
    else if (i == 3) {
        div();
    }
}
function operate2() {
    var er2 = document.getElementById("error2");
    var tt3 = document.getElementById("tt3");
    var tt1 = document.getElementById("tt1");
    var ang = parseFloat(tt1.value);
    er2.innerHTML = "";
    if (isNaN(ang)) {
        er2.innerHTML = "Don't enter String";
    }
    var ele = document.getElementById('func2');
    var i = ele.selectedIndex;
    if (i == 0) {
        tt3.value = Math.sin(Math.PI * ang / 360).toString();
    }
    else if (i == 1) {
        tt3.value = Math.cos(Math.PI * ang / 360).toString();
    }
    else if (i == 2) {
        tt3.value = Math.tan(Math.PI * ang / 360).toString();
    }
    else if (i == 3) {
        tt3.value = Math.sqrt(ang).toString();
    }
}
//# sourceMappingURL=app.js.map