function areaC()
{
    let t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    let ans:HTMLElement=<HTMLElement>document.getElementById("ans");
    var s=t1.value;
    var si=s.toLowerCase().indexOf('e');
    if(si<0)
        ans.innerHTML=" \'e\' or \'E\' is not present"+"<br />";
    else
        ans.innerHTML="First Index of \'e\' or \'E\' is "+si+"<br />";
    
    ans.innerHTML+="Right Half substring : "+s.substr(s.length/2);
}