function areaC() {
    let t1 = document.getElementById("t1");
    let ans = document.getElementById("ans");
    var s = t1.value;
    var u = s.toUpperCase();
    var l = s.toLowerCase();
    var spl = s.split(' ');
    ans.innerHTML = "Lowercase : " + l + "<br />" + "Uppercase : " + u + "<br/>";
    ans.innerHTML += "Space seperated strings: [ ";
    for (var i = 0; i < spl.length; i++) {
        ans.innerHTML += spl[i] + ", ";
    }
    ans.innerHTML += " ]";
}
//# sourceMappingURL=math3.js.map