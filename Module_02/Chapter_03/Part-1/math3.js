function areaC() {
    let t11 = document.getElementById("t11");
    let ans = document.getElementById("ans");
    var a = parseFloat(t11.value);
    if (isNaN(a)) {
        ans.innerHTML = "Don't Enter String";
        ans.style.color = "red";
    }
    else {
        if (a % 2 == 0)
            ans.innerHTML = "The Number is Even.";
        else
            ans.innerHTML = "The Number is Odd.";
    }
}
//# sourceMappingURL=math3.js.map