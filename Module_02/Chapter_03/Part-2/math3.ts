function areaC()
{
    let t11:HTMLInputElement=<HTMLInputElement>document.getElementById("t11");
    let t12:HTMLInputElement=<HTMLInputElement>document.getElementById("t12");
    let t21:HTMLInputElement=<HTMLInputElement>document.getElementById("t21");
    let t22:HTMLInputElement=<HTMLInputElement>document.getElementById("t22");
    let t31:HTMLInputElement=<HTMLInputElement>document.getElementById("t31");
    let t32:HTMLInputElement=<HTMLInputElement>document.getElementById("t32");
    
    let ans:HTMLElement=<HTMLElement>document.getElementById("ans");
    var x1=parseFloat(t11.value),y1=parseFloat(t12.value),x2=parseFloat(t21.value),y2=parseFloat(t22.value),x3=parseFloat(t31.value),y3=parseFloat(t32.value);
    if(isNaN(x1) || isNaN(x2) || isNaN(x3) || isNaN(y1) || isNaN(y2) || isNaN(y3))
    {
        //ans.innerHTML="Don't Enter any String";    
        //ans.style.color = "red";
        ////Error Can also be handled by Uncommenting aboving code.
        alert("Don't Enter any String");
    }
    else
    {
        var x=Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2)),y=Math.sqrt(Math.pow(x2-x3,2)+Math.pow(y2-y3,2)),z=Math.sqrt(Math.pow(x3-x1,2)+Math.pow(y3-y1,2));
        ans.innerHTML = "";

        if(x==y && y==z)
        ans.innerHTML = "Its Equilateral Triangle.";
        else if(x==y || y==z || x==z)
        ans.innerHTML = "Its Isosceles Triangle.";
        else
        ans.innerHTML = "Its Neither Isosceles Nor Equilateral Triangle.";
        
        if(  z*z==x*x+y*y || x*x==y*y+z*z  || y*y==z*z+x*x )
        ans.innerHTML = "Its Right-Angled Triangle.";
        // console.log(x); //Uncomment to debug
        // console.log(y);
        // console.log(z);
        ans.style.color = "black";
    }
}