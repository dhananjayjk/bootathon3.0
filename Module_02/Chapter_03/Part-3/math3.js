function areaC() {
    let t1 = document.getElementById("t1");
    let ans = document.getElementById("ans");
    var s = t1.value;
    s = s.split(' ').join(''); // To remove whitespaces of string 5 + 6 i to 5+6i
    console.log(s);
    var signn = s.lastIndexOf('-');
    var signp = s.lastIndexOf('+');
    var sign = Math.max(signn, signp);
    var isneg = false; //Flag to handle negative complex part.
    if (sign == signn)
        isneg = true;
    var n1, n2;
    if (sign > 0) {
        var s1 = s.substr(0, sign);
        var s2 = s.substr(sign + 1, s.length - 1);
        console.log(s1);
        console.log(s2);
        n1 = parseFloat(s1);
        n2 = parseFloat(s2);
        if (isneg)
            n2 = -n2;
        if (isNaN(n1) || isNaN(n2))
            alert("Enter valid numbers and not alphabets");
        else
            ans.innerHTML = "Real Part : " + n1 + "  |  Complex Part : " + n2;
    }
    else {
        n1 = parseFloat(s);
        if (isNaN(n1))
            alert("Enter valid numbers and not alphabets");
        else
            ans.innerHTML = "Real Part : " + n1 + "  |  Complex Part : " + 0;
    }
}
//# sourceMappingURL=math3.js.map