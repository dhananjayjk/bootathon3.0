function calc(x1, y1, x2, y2, x3, y3) {
    var x = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)), y = Math.sqrt(Math.pow(x2 - x3, 2) + Math.pow(y2 - y3, 2)), z = Math.sqrt(Math.pow(x3 - x1, 2) + Math.pow(y3 - y1, 2));
    var s = (x + y + z) / 2;
    return Math.sqrt(s * (s - x) * (s - y) * (s - z));
}
function areaC() {
    let t11 = document.getElementById("t11");
    let t12 = document.getElementById("t12");
    let t21 = document.getElementById("t21");
    let t22 = document.getElementById("t22");
    let t31 = document.getElementById("t31");
    let t32 = document.getElementById("t32");
    let t41 = document.getElementById("t41");
    let t42 = document.getElementById("t42");
    let ans = document.getElementById("ans");
    var x1 = parseFloat(t11.value), y1 = parseFloat(t12.value), x2 = parseFloat(t21.value), y2 = parseFloat(t22.value), x3 = parseFloat(t31.value), y3 = parseFloat(t32.value), px1 = parseFloat(t41.value), py1 = parseFloat(t42.value);
    if (isNaN(x1) || isNaN(x2) || isNaN(x3) || isNaN(y1) || isNaN(y2) || isNaN(y3) || isNaN(px1) || isNaN(py1)) {
        ans.innerHTML = "Don't Enter any String";
        ans.style.color = "red";
    }
    else {
        var x = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)), y = Math.sqrt(Math.pow(x2 - x3, 2) + Math.pow(y2 - y3, 2)), z = Math.sqrt(Math.pow(x3 - x1, 2) + Math.pow(y3 - y1, 2));
        var a1 = calc(x1, y1, x2, y2, px1, py1), a2 = calc(x1, y1, x3, y3, px1, py1), a3 = calc(x2, y2, x3, y3, px1, py1), a = calc(x1, y1, x2, y2, x3, y3);
        if (Math.abs(a1 + a2 + a3 - a) < Number.EPSILON)
            ans.innerHTML = "Point is in Triangle";
        else
            ans.innerHTML = "Point is not in Triangle";
        ans.style.color = "black";
    }
}
//# sourceMappingURL=math3.js.map