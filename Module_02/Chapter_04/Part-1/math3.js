function areaC() {
    let ans = document.getElementById("ans");
    var a = 0, b = 0, c = 0;
    var n = window.prompt("Enter number of inputs.");
    var k = parseInt(n);
    if (isNaN(k)) {
        alert("Do not enter alphabets.");
    }
    for (var i = 0; i < k; i++) {
        var x = parseInt(window.prompt("Enter " + (i + 1) + "th number."));
        if (isNaN(x)) {
            alert("Do not enter alphabets.");
            i--;
            continue;
        }
        else {
            if (x == 0)
                a++;
            else if (x > 0)
                b++;
            else
                c++;
        }
    }
    ans.innerHTML = "Positive : " + b + "<br />" + "Negative: " + c + "<br />" + "Zeroes: " + a;
}
//# sourceMappingURL=math3.js.map