function arm(n) {
    var ans = false;
    var a = 0;
    var x = n.toString().split('');
    for (var i = 0; i < x.length; i++) {
        var t = parseInt(x[i]);
        a += t * t * t;
    }
    if (a == n)
        ans = true;
    return ans;
}
function areaC() {
    let ans = document.getElementById("ans");
    ans.innerHTML = "Armstrong numbers from 100 to 900 are ";
    for (var i = 100; i <= 900; i++) {
        if (arm(i)) {
            ans.innerHTML += i + ", ";
        }
    }
}
//# sourceMappingURL=math3.js.map