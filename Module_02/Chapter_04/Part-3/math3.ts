
function areaC()
{
    let ans:HTMLElement=<HTMLElement>document.getElementById("ans");
    let t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var n=parseInt(t1.value);
    if(isNaN(n))
    {
        alert("Don't enter alphabets.")
    }
    else
    {
        var table: HTMLTableElement = <HTMLTableElement> document.getElementById("myTable");
    
        var rowr = table.insertRow(0);
        var cell1 = rowr.insertCell(0);
        var cell2 = rowr.insertCell(1);
        var cell3 = rowr.insertCell(2);
        var cell4 = rowr.insertCell(3);
        var cell5 = rowr.insertCell(4);
        
        cell1.innerHTML = "Number";
        cell2.innerHTML = "*";
        cell3.innerHTML = "Multiple";
        cell4.innerHTML = "=";
        cell5.innerHTML = "Table";

        for(var i=1;i<=10;i++)
        {
            var rowr = table.insertRow(i);
            var cell1 = rowr.insertCell(0);
            var cell2 = rowr.insertCell(1);
            var cell3 = rowr.insertCell(2);
            var cell4 = rowr.insertCell(3);
            var cell5 = rowr.insertCell(4);
            
            cell1.innerHTML = n.toString();
            cell2.innerHTML = "*";
            cell3.innerHTML = i.toString();
            cell4.innerHTML = "=";
            cell5.innerHTML = (n*i).toString();

        }
        table.border="1px solid black";
        table.align="center";
    }
}